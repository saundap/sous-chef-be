package com.birchfield.services.restfulwebservicejwtauth.todo;

import com.birchfield.services.restfulwebservicejwtauth.BaseTest;
import com.birchfield.services.restfulwebservicejwtauth.TestUtils;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;


//https://spring.io/guides/gs/testing-web/
//Tests via HTTP layer
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
class HTTPRequestTest extends BaseTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    @Tag("IntegrationTest")
    @Order(1)
    public void getListTodos() throws URISyntaxException {
        HttpEntity<String> request = TestUtils.setRequestWithBearerToken(jwtTokenUtil, null);
        URI uri = new URI("http://localhost:" + port + "/users/andy/todos");
        ResponseEntity<String> response = this.restTemplate.exchange(uri, HttpMethod.GET, request, String.class);

        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertEquals(true, response.getBody().contains("Learn to Dance"));
    }

    @Test
    @Tag("IntegrationTest")
    @Order(2)
    public void deleteFromListTodos() throws URISyntaxException {
         HttpEntity<String> request = TestUtils.setRequestWithBearerToken(jwtTokenUtil, null);
        URI uri = new URI("http://localhost:" + port + "/users/andy/todo/1");
        ResponseEntity<String> delResp = this.restTemplate.exchange(uri, HttpMethod.DELETE, request, String.class);
        Assert.assertEquals(204, delResp.getStatusCodeValue());

        ResponseEntity<String> getResp = this.restTemplate.exchange("http://localhost:" + port + "/users/andy/todos",
                HttpMethod.GET,
                request,
                String.class);

        Assert.assertEquals(200, getResp.getStatusCodeValue());
        Assert.assertEquals(false, getResp.getBody().contains("Learn to Dance"));
    }

    @Test
    @Tag("IntegrationTest")
    @Order(3)
    public void getTodo() throws URISyntaxException {
        HttpEntity<String> request = TestUtils.setRequestWithBearerToken(jwtTokenUtil, null);
        URI uri = new URI("http://localhost:" + port + "/users/andy/todo/5");
        ResponseEntity<String> response = this.restTemplate.exchange(uri, HttpMethod.GET, request, String.class);

        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertEquals(true, response.getBody().contains("SpringBoot"));
    }

    @Test
    @Tag("IntegrationTest")
    @Order(4)
    public void updateTodo() throws URISyntaxException {
        Todo todo = new Todo(5, "andy", "Update Test", new Date(), false);
        HttpEntity<String> request = TestUtils.setRequestWithBearerToken(jwtTokenUtil, todo);
        URI uriPUT = new URI("http://localhost:" + port + "/users/andy/todo/5");
        ResponseEntity<String> response = this.restTemplate.exchange(uriPUT,
                HttpMethod.PUT,
                request,
                String.class);
        System.out.println(response.toString());
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertEquals(false, response.getBody().contains("SpringBoot")); //updated record former desc
        Assert.assertEquals(true, response.getBody().contains("Update Test"));

        URI uriGET = new URI("http://localhost:" + port + "/users/andy/todos");
        ResponseEntity<String> getResp = this.restTemplate.exchange(uriGET,
                HttpMethod.GET,
                request,
                String.class);
        Assert.assertEquals(200, getResp.getStatusCodeValue());
        Assert.assertEquals(true, getResp.getBody().contains("Update Test"));
    }

    @Test
    @Tag("IntegrationTest")
    @Order(5)
    public void createATodo() throws URISyntaxException {
        JSONObject obj = new JSONObject();
        obj.put("username", "andy");
        obj.put("description", "Learn to Ride a Bicycle");
        obj.put("targetDate", 1607969575);
        obj.put("done", false);
        HttpEntity<String> request = TestUtils.setRequestWithBearerToken(jwtTokenUtil, obj.toString());
        URI uriPOST = new URI("http://localhost:" + port + "/users/andy/todo");

        ResponseEntity<String> postResp = this.restTemplate.exchange(uriPOST,
                HttpMethod.POST,
                request,
                String.class);
        Assert.assertEquals(201, postResp.getStatusCodeValue());

        URI uriGET = new URI(postResp.getHeaders().getLocation().toString()); //Location Header Returned should give us the created todo
        ResponseEntity<String> getResp = this.restTemplate.exchange(uriGET,
                HttpMethod.GET,
                request,
                String.class);
        Assert.assertEquals(200, getResp.getStatusCodeValue());
        Assert.assertEquals(true, getResp.getBody().contains("Bicycle"));
    }
}