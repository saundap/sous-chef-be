package com.birchfield.services.restfulwebservicejwtauth.todo;

//Using MVCMock
//useful approach is to not start the server at all but to test only the
// layer below that, where Spring handles the incoming HTTP request and
// hands it off to your controller. That way, almost of the full
// stack is used, and your code will be called in exactly the same
// way as if it were processing a real HTTP request but without the
// cost of starting the server.
//https://spring.io/guides/gs/testing-web/
/*********
//narrow the tests to only the web layer by using @WebMvcTest,
 The test assertion is the same as in the previous case (WebApplicationTestFullSpringContext).
 However, in this test, Spring Boot instantiates only the web layer rather
 than the whole context. In an application with multiple controllers, you
 can even ask for only one to be instantiated by using, for example,
 @WebMvcTest(HomeController.class).

 @WebMvcTest is only going to scan the controller you've defined and the MVC infrastructure.
 That's it. So if your controller has some dependency to other beans from your service layer,
 the test won't start until you either load that config yourself or provide a mock for it. This
 is much faster as we only load a tiny portion of your app. This annotation uses slicing.
*/

import com.birchfield.services.restfulwebservicejwtauth.BaseTest;
import com.birchfield.services.restfulwebservicejwtauth.auth.jwt.JWTWebSecurityConfig;
import com.birchfield.services.restfulwebservicejwtauth.auth.jwt.JwtTokenUtil;
import com.birchfield.services.restfulwebservicejwtauth.auth.jwt.JwtUnAuthorizedResponseAuthenticationEntryPoint;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.birchfield.services.restfulwebservicejwtauth.TestUtils.asJsonString;
import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//Spring Web Handling Layer Test
@WebMvcTest(TodoController.class)
@AutoConfigureMockMvc(addFilters = false)
public class WebApplicationTestWebLayerSpringContext extends BaseTest {
    final Logger logger = LoggerFactory.getLogger(WebApplicationTestWebLayerSpringContext.class);

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private JWTWebSecurityConfig jwtWebSecurityConfig;

    @MockBean
    private JwtUnAuthorizedResponseAuthenticationEntryPoint jwtUnAuthorizedResponseAuthenticationEntryPoint;

    @MockBean
    private UserDetailsService userDetailsService;

    @MockBean
    private JwtTokenUtil jwtTokenUtil;

    @MockBean
    private TodoHardCodedService mockService;

    @Test
    @Tag("UnitTest")
    public void shouldReturnListofTodos() throws Exception {
        List<Todo> todos = new ArrayList();
        int idCounter = 0;
            todos.add(new Todo(idCounter++, "andy", "Learn about Data", new Date(), false));
            todos.add(new Todo(idCounter++, "andy", "Learn about JPA", new Date(), false));
            todos.add(new Todo(idCounter++, "andy", "Learn about MockBeans", new Date(), false));

        when(mockService.findAll()).thenReturn(todos);
        this.mockMvc.perform(get("/users/andy/todos")).andDo(print())
                .andExpect(status().isOk())
        .andExpect(content().string(containsString("Learn about MockBeans")));

        verify(mockService, atLeast(1)).findAll();
    }

    @Test
    @Tag("UnitTest")
    public void shouldReturnATodo() throws Exception {
        Todo todo = new Todo(1, "andy", "Learn about Data", new Date(), false);

        when(mockService.findById(1)).thenReturn(todo);
        this.mockMvc.perform(get("/users/andy/todo/1")).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Learn about Data")));

        verify(mockService, atLeast(1)).findById(1);
    }

    @Test
    @Tag("UnitTest")
    public void deleteATodo() throws Exception {
        Todo todo = new Todo(1, "andy", "Learn to Dance", new Date(), false);
        when(mockService.deleteById(1)).thenReturn(todo);
        this.mockMvc.perform(delete("/users/andy/todo/1")).andDo(print())
                .andExpect(status().isNoContent());

        verify(mockService, atLeast(1)).deleteById(1);
    }

    @Test
    @Tag("UnitTest")
    public void updateATodo() throws Exception {
        Todo todo = new Todo(4, "andy", "Update Test", new Date(), false);

        when(mockService.save(todo)).thenReturn(todo);

        this.mockMvc.perform(put("/users/andy/todo/4")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(todo))
        ).andDo(print())
        .andExpect(status().isOk())
        .andDo(print())
        .andExpect(content().string(containsString("Update Test")));

        verify(mockService, atLeast(1)).save(todo);

    }

    @Test
    @Tag("UnitTest")
    public void doNotUpdateATodoWhereIdsDoNotMatch() throws Exception {
        Todo todo = new Todo(4, "andy", "Update Test", new Date(), false);

        when(mockService.save(todo)).thenReturn(todo);

        this.mockMvc.perform(put("/users/andy/todo/2")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(todo)))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andDo(print())
                .andExpect(content().string(containsString("The id in the URI did not match the id in the request")));
    }

    @Test
    @Tag("UnitTest")
    public void createATodo() throws Exception {
        Todo todo = new Todo(10, "andy", "Learn to Sail a Boat", new Date(), false);

        when(mockService.save(todo)).thenReturn(todo);

        this.mockMvc.perform(post("/users/andy/todo")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(todo)))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.header().exists("location"));

        verify(mockService, atLeast(1)).save(todo);

    }
}
