package com.birchfield.services.restfulwebservicejwtauth;

import com.birchfield.services.restfulwebservicejwtauth.auth.jwt.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;

public class BaseTest {
    protected static final String USERNAME = "andy";
    protected static final String PASSWORD = "password";
    protected static final String ROLE = "test_role";

    @Autowired
    protected JwtTokenUtil jwtTokenUtil;

}
