package com.birchfield.services.restfulwebservicejwtauth.helloworld;

import com.birchfield.services.restfulwebservicejwtauth.BaseTest;
import com.birchfield.services.restfulwebservicejwtauth.TestUtils;
import org.junit.Assert;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.net.URI;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

//Tests via HTTP layer
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class HTTPRequestTest extends BaseTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    @Tag("IntegrationTest")
    public void greetingShouldReturnDefaultMessage() throws Exception {
        HttpEntity<String> request = TestUtils.setRequestWithBearerToken(jwtTokenUtil, null);
        URI uri = new URI("http://localhost:" + port + "/helloworld");
        ResponseEntity<String> response = this.restTemplate.exchange(uri, HttpMethod.GET, request, String.class);

        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertEquals(true, response.getBody().contains("Hello World"));
    }

    @Test
    @Tag("IntegrationTest")
    public void greetingShouldReturnDefaultObject() throws Exception {
        HttpEntity<String> request = TestUtils.setRequestWithBearerToken(jwtTokenUtil, null);
        URI uri = new URI("http://localhost:" + port + "/helloworldbean");
        ResponseEntity<String> response = this.restTemplate.exchange(uri, HttpMethod.GET, request, String.class);

        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertEquals("{\"message\":\"Hello World Bean\"}", response.getBody());
    }

    @Test
    @Tag("IntegrationTest")
    public void greetingShouldReturnDefaultObjectPathVariable() throws Exception {
        HttpEntity<String> request = TestUtils.setRequestWithBearerToken(jwtTokenUtil, null);
        URI uri = new URI("http://localhost:" + port + "/helloworldbean/andy");
        ResponseEntity<String> response = this.restTemplate.exchange(uri, HttpMethod.GET, request, String.class);

        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertEquals("{\"message\":\"Hello World Bean andy\"}", response.getBody());
    }
}