package com.birchfield.services.restfulwebservicejwtauth;

import com.birchfield.services.restfulwebservicejwtauth.auth.jwt.JwtTokenUtil;
import com.birchfield.services.restfulwebservicejwtauth.auth.jwt.JwtUserDetails;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;

public class TestUtils extends BaseTest {

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static HttpEntity<String> setRequestWithBearerToken(JwtTokenUtil jwtTokenUtil, Object body){
        String token = generateToken(jwtTokenUtil, USERNAME, PASSWORD, ROLE);

        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Bearer "+token);
        headers.set("Content-Type", "application/json");

        if(body == null){
            return new HttpEntity(headers);

        }else{
            return new HttpEntity(body, headers);
        }
    }

    public static String generateToken(JwtTokenUtil jwtTokenUtil, String user, String password, String role){
        JwtUserDetails jwtUserDetails = new JwtUserDetails(1L, user, password, role);
        return jwtTokenUtil.generateToken(jwtUserDetails);
    }
}
