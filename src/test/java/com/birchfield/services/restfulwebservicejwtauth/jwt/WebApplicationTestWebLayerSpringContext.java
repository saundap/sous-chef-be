package com.birchfield.services.restfulwebservicejwtauth.jwt;

//Using MVCMock
//useful approach is to not start the server at all but to test only the
// layer below that, where Spring handles the incoming HTTP request and
// hands it off to your controller. That way, almost of the full
// stack is used, and your code will be called in exactly the same
// way as if it were processing a real HTTP request but without the
// cost of starting the server.

/*********
//narrow the tests to only the web layer by using @WebMvcTest,
 The test assertion is the same as in the previous case (WebApplicationTestFullSpringContext).
 However, in this test, Spring Boot instantiates only the web layer rather
 than the whole context. In an application with multiple controllers, you
 can even ask for only one to be instantiated by using, for example,
*/

import com.birchfield.services.restfulwebservicejwtauth.BaseTest;
import com.birchfield.services.restfulwebservicejwtauth.auth.jwt.JWTWebSecurityConfig;
import com.birchfield.services.restfulwebservicejwtauth.auth.jwt.JwtTokenUtil;
import com.birchfield.services.restfulwebservicejwtauth.auth.jwt.JwtUnAuthorizedResponseAuthenticationEntryPoint;
import com.birchfield.services.restfulwebservicejwtauth.helloworld.HelloWorldController;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//Spring Web Handling Layer Test
@WebMvcTest(HelloWorldController.class)
@AutoConfigureMockMvc(addFilters = false)
public class WebApplicationTestWebLayerSpringContext extends BaseTest {
    final Logger logger = LoggerFactory.getLogger(com.birchfield.services.restfulwebservicejwtauth.todo.WebApplicationTestWebLayerSpringContext.class);

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private JWTWebSecurityConfig jwtWebSecurityConfig;

    @MockBean
    private JwtUnAuthorizedResponseAuthenticationEntryPoint jwtUnAuthorizedResponseAuthenticationEntryPoint;

    @MockBean
    private UserDetailsService userDetailsService;

    @MockBean
    private JwtTokenUtil jwtTokenUtil;

    @Test
    @Tag("UnitTest")
    public void shouldBeAuthorised() throws Exception {
        this.mockMvc.perform(get("/helloworld")).andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().string(containsString("Hello World")));
    }
}
