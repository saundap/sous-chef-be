package com.birchfield.services.restfulwebservicejwtauth.jwt;

import com.birchfield.services.restfulwebservicejwtauth.BaseTest;
import com.birchfield.services.restfulwebservicejwtauth.auth.jwt.JwtTokenUtil;
import com.birchfield.services.restfulwebservicejwtauth.TestUtils;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.net.URI;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;

//Tests via HTTP layer
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class HTTPRequestTest extends BaseTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Test
    @Tag("IntegrationTest")
    public void retrieveTokenSuccessful() throws Exception {
        final String baseUrl = "http://localhost:" + port + "/authenticate";
        URI uri = new URI(baseUrl);

        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");

        JSONObject body = new JSONObject();
        body.put("username",USERNAME);
        body.put("password",PASSWORD);

        HttpEntity<JSONObject> request = new HttpEntity(body.toString(), headers);

        ResponseEntity<String> response = this.restTemplate.postForEntity(uri, request, String.class);

        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertEquals(true, response.getBody().contains("token"));
    }

    @Test
    @Tag("IntegrationTest")
    public void useTokenSuccessful() throws Exception {
        String token = TestUtils.generateToken(jwtTokenUtil, USERNAME, PASSWORD, ROLE);
        final String baseUrl = "http://localhost:" + port + "/users/andy/todos";
        URI uri = new URI(baseUrl);

        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Bearer "+token);
        headers.set("Content-Type", "application/json");

        JSONObject body = new JSONObject();
        body.put("username",USERNAME);
        body.put("password",PASSWORD);

        HttpEntity<JSONObject> request = new HttpEntity(body.toString(), headers);
        ResponseEntity<String> response = this.restTemplate.exchange(uri, HttpMethod.GET, request, String.class);

        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertEquals(true, response.getBody().contains("Learn to Cook"));
    }

    @Test
    @Tag("IntegrationTest")
    public void retrieveTokenFailWrongContentType() throws Exception {
        final String baseUrl = "http://localhost:" + port + "/authenticate";
        URI uri = new URI(baseUrl);

        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/text");

        JSONObject body = new JSONObject();
        body.put("username",USERNAME);
        body.put("password",PASSWORD);

        HttpEntity<JSONObject> request = new HttpEntity(body.toString(), headers);

        ResponseEntity<String> response = this.restTemplate.postForEntity(uri, request, String.class);
        System.out.println(response.getBody());
        Assert.assertEquals(401, response.getStatusCodeValue());
        Assert.assertNull(response.getBody());
    }

    @Test
    @Tag("IntegrationTest")
    public void retrieveTokenFailNonexistentUser() throws Exception {
        final String baseUrl = "http://localhost:" + port + "/authenticate";
        URI uri = new URI(baseUrl);

        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");

        JSONObject body = new JSONObject();
        body.put("username","nonexistentuser");
        body.put("password",PASSWORD);

        System.out.println(body.toString());

        HttpEntity<JSONObject> request = new HttpEntity(body.toString(), headers);

        ResponseEntity<String> response = this.restTemplate.postForEntity(uri, request, String.class);

        Assert.assertEquals(401, response.getStatusCodeValue());
        Assert.assertTrue(Objects.requireNonNull(response.getBody()).contains("INVALID_CREDENTIALS"));
    }

    @Test
    @Tag("IntegrationTest")
    public void retrieveTokenFailInvalidPassword() throws Exception {
        final String baseUrl = "http://localhost:" + port + "/authenticate";
        URI uri = new URI(baseUrl);

        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");

        JSONObject body = new JSONObject();
        body.put("username",USERNAME);
        body.put("password","invalid_password");

        HttpEntity<JSONObject> request = new HttpEntity(body.toString(), headers);

        ResponseEntity<String> response = this.restTemplate.postForEntity(uri, request, String.class);

        Assert.assertEquals(401, response.getStatusCodeValue());
        Assert.assertTrue(Objects.requireNonNull(response.getBody()).contains("INVALID_CREDENTIALS"));
    }

    @Test
    @Tag("IntegrationTest")
    public void unauthorisedNoHeaders() throws Exception {
        final String baseUrl = "http://localhost:" + port + "/users/todos/andy";
        URI uri = new URI(baseUrl);
        assertThat(this.restTemplate.getForObject(uri, String.class)).contains("Unauthorized");
    }

    @Test
    @Tag("IntegrationTest")
    public void unauthorisedWithBasicAuth() throws Exception {
        assertThat(this.restTemplate.withBasicAuth(USERNAME, PASSWORD).getForObject("http://localhost:" + port + "/authenticate",
                String.class)).contains("Unauthorized");
    }



}
