package com.birchfield.services.restfulwebservicejwtauth.jwt;

import org.junit.jupiter.api.Test;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import java.util.ArrayList;


public class BcryptEncoderTest {

    @Test
    public void testSpringBCryptEncoder(){
        String expectedPassword = "password";

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        ArrayList<String> encodedPasswords = new ArrayList<String>();
        for(int i=1; i<=10; i++){
            String encodedString = encoder.encode(expectedPassword);
            encodedPasswords.add(encodedString);
            System.out.println(encodedString);
        }

        // Check that an unencrypted password matches one that has
        // previously been hashed
        for (String actual_password : encodedPasswords) {
            assert(BCrypt.checkpw(expectedPassword, actual_password));
        }


    }
}
