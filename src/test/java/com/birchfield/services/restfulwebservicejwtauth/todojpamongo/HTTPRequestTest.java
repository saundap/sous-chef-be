package com.birchfield.services.restfulwebservicejwtauth.todojpamongo;

import com.birchfield.services.restfulwebservicejwtauth.BaseTest;
import com.birchfield.services.restfulwebservicejwtauth.TestUtils;
import com.birchfield.services.restfulwebservicejwtauth.todojpah2.TodoJPA;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBObject;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;

import javax.sound.midi.SysexMessage;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


//https://spring.io/guides/gs/testing-web/
//Tests via HTTP layer
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
//@DataMongoTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
class HTTPRequestTest extends BaseTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @BeforeAll()
    private static void loadData(@Autowired MongoTemplate mongoTemplate){
        //List<DBObject> objects = new ArrayList<>();
        // given
        DBObject objectToSave1 = BasicDBObjectBuilder.start()
                .add("id", 10001)
                .add("username", "andy")
                .add("description", "Learn about SpringBoot")
                .add("targetDate", new Date())
                .add("isDone", false)
                .get();

        // when
        mongoTemplate.save(objectToSave1, "todo");

        DBObject objectToSave2 = BasicDBObjectBuilder.start()
                .add("id", 10002)
                .add("username", "andy")
                .add("description", "Learn about JPA")
                .add("targetDate", new Date())
                .add("isDone", false)
                .get();

        mongoTemplate.save(objectToSave2, "todo");

        DBObject objectToSave3 = BasicDBObjectBuilder.start()
                .add("id", 10003)
                .add("username", "andy")
                .add("description", "Learn about React")
                .add("targetDate", new Date())
                .add("isDone", false)
                .get();

        mongoTemplate.save(objectToSave3, "todo");

    }

    @Test
    @Tag("IntegrationTest")
    public void getListTodos() throws URISyntaxException {
        HttpEntity<String> request = TestUtils.setRequestWithBearerToken(jwtTokenUtil, null);
        URI uri = new URI("http://localhost:" + port + "/jpa/mongo/users/andy/todos");
        ResponseEntity<String> response = this.restTemplate.exchange(uri, HttpMethod.GET, request, String.class);

        System.out.println(response.toString());
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertEquals(true, response.getBody().contains("SpringBoot"));
    }

    @Test
    @Tag("IntegrationTest")
    public void deleteFromListTodos() throws URISyntaxException {
        HttpEntity<String> request = TestUtils.setRequestWithBearerToken(jwtTokenUtil, null);
        URI uri = new URI("http://localhost:" + port + "/jpa/mongo/users/andy/todo/10002");
        ResponseEntity<String> delResp = this.restTemplate.exchange(uri, HttpMethod.DELETE, request, String.class);
        Assert.assertEquals(204, delResp.getStatusCodeValue());

        ResponseEntity<String> getResp = this.restTemplate.exchange("http://localhost:" + port + "/jpa/mongo/users/andy/todos",
                HttpMethod.GET,
                request,
                String.class);

        System.out.println(getResp.toString());
        Assert.assertEquals(200, getResp.getStatusCodeValue());
        Assert.assertEquals(false, getResp.getBody().contains("Learn about JPA"));
    }

    @Test
    @Tag("IntegrationTest")
    public void getTodo() throws URISyntaxException {
        HttpEntity<String> request = TestUtils.setRequestWithBearerToken(jwtTokenUtil, null);
        URI uri = new URI("http://localhost:" + port + "/jpa/mongo/users/andy/todo/10001");
        ResponseEntity<String> response = this.restTemplate.exchange(uri, HttpMethod.GET, request, String.class);

        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertEquals(true, response.getBody().contains("Learn about SpringBoot"));
    }

    @Test
    @Tag("IntegrationTest")
    public void updateTodo() throws URISyntaxException {
        TodoJPAMongo todo = new TodoJPAMongo(10003, "andy", "Update Test", new Date(), false);
        HttpEntity<String> request = TestUtils.setRequestWithBearerToken(jwtTokenUtil, todo);
        URI uriPUT = new URI("http://localhost:" + port + "/jpa/mongo/users/andy/todo/10003");
        ResponseEntity<String> response = this.restTemplate.exchange(uriPUT,
                HttpMethod.PUT,
                request,
                String.class);

        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertEquals(false, response.getBody().contains("Learn about React")); //updated record former desc
        Assert.assertEquals(true, response.getBody().contains("Update Test"));

        URI uriGET = new URI("http://localhost:" + port + "/jpa/mongo/users/andy/todos");
        ResponseEntity<String> getResp = this.restTemplate.exchange(uriGET,
                HttpMethod.GET,
                request,
                String.class);
        Assert.assertEquals(200, getResp.getStatusCodeValue());
        Assert.assertEquals(true, getResp.getBody().contains("Update Test"));
    }

    @Test
    @Tag("IntegrationTest")
    public void createATodo() throws URISyntaxException {
        JSONObject obj = new JSONObject();
        obj.put("username", "andy");
        obj.put("description", "Learn to Ride a Bicycle");
        obj.put("targetDate", 1607969575);
        obj.put("done", false);
        HttpEntity<String> request = TestUtils.setRequestWithBearerToken(jwtTokenUtil, obj.toString());
        URI uriPOST = new URI("http://localhost:" + port + "/jpa/mongo/users/andy/todo");

        ResponseEntity<String> postResp = this.restTemplate.exchange(uriPOST,
                HttpMethod.POST,
                request,
                String.class);
        Assert.assertEquals(201, postResp.getStatusCodeValue());

        URI uriGET = new URI(postResp.getHeaders().getLocation().toString()); //Location Header Returned should give us the created todo
        ResponseEntity<String> getResp = this.restTemplate.exchange(uriGET,
                HttpMethod.GET,
                request,
                String.class);
        Assert.assertEquals(200, getResp.getStatusCodeValue());
        Assert.assertEquals(true, getResp.getBody().contains("Bicycle"));
    }
}