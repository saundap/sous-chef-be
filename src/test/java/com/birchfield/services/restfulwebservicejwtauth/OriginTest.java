package com.birchfield.services.restfulwebservicejwtauth;

import com.birchfield.services.restfulwebservicejwtauth.auth.jwt.JWTWebSecurityConfig;
import com.birchfield.services.restfulwebservicejwtauth.auth.jwt.JwtTokenUtil;
import com.birchfield.services.restfulwebservicejwtauth.todo.TodoHardCodedService;
import com.birchfield.services.restfulwebservicejwtauth.todo.TodoController;
import org.junit.jupiter.api.Disabled;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.options;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//Spring Web Handling Layer Test
@WebMvcTest(TodoController.class)
public class OriginTest extends BaseTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private JWTWebSecurityConfig jwtWebSecurityConfig;

    @MockBean
    private UserDetailsService userDetailsService;

    @MockBean
    private JwtTokenUtil jwtTokenUtil;

    @MockBean
    private TodoHardCodedService service;

    @Disabled
    public void shouldAllowCrossOrigin() throws Exception {
        ResultActions actions = mockMvc.perform(
                get("/users/test/todos")
                        .contentType(MediaType.APPLICATION_JSON)
                        //CORS HEADERS
                        //.header("Access-Control-Request-Method", "GET")
                        .header("Origin", "http://localhost:4200")
                        .header("Authorization", "Bearer " + TestUtils.generateToken(jwtTokenUtil, USERNAME, PASSWORD, ROLE))
        );
        actions.andExpect(status().isOk());
    }

    @Disabled
    public void notAllowCrossOrigin() throws Exception {
        ResultActions actions = mockMvc.perform(
                get("/users/test/todos")
                        .contentType(MediaType.APPLICATION_JSON)
                        //CORS HEADERS
                        //.header("Access-Control-Request-Method", "GET")
                        .header("Origin", "http://anotherdomain:4200")
                        .header("Authorization", "Bearer " + TestUtils.generateToken(jwtTokenUtil, USERNAME, PASSWORD, ROLE))
                );
        actions.andExpect(status().isForbidden());
    }
}
