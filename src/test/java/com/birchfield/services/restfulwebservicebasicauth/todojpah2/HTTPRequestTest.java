package com.birchfield.services.restfulwebservicebasicauth.todojpah2;

import com.birchfield.services.restfulwebservicebasicauth.todo.Todo;
import org.json.JSONObject;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.annotation.DirtiesContext;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;


//https://spring.io/guides/gs/testing-web/
//Tests via HTTP layer
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
class HTTPRequestTest {

    private static String USERNAME = "andy";
    private static String PASSWORD = "password";

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    @Tag("IntegrationTest")
    @Order(1)
    public void get401InvalidUser() {
        //https://www.baeldung.com/spring-security-integration-tests
        ResponseEntity<String> result = restTemplate.withBasicAuth("invaliduser", PASSWORD)
                .getForEntity("/jpa/users/andy/todos", String.class);
        assertEquals(HttpStatus.UNAUTHORIZED, result.getStatusCode());
    }

    @Test
    @Tag("IntegrationTest")
    @Order(1)
    public void get401InvalidPassword() {
        //https://www.baeldung.com/spring-security-integration-tests
        ResponseEntity<String> result = restTemplate.withBasicAuth(USERNAME, "invalidpassword")
                .getForEntity("/jpa/users/andy/todos", String.class);
        assertEquals(HttpStatus.UNAUTHORIZED, result.getStatusCode());
    }

    @Test
    @Tag("IntegrationTest")
    @Order(1)
    public void getListTodos() {
        assertThat(this.restTemplate.withBasicAuth(USERNAME, PASSWORD).getForObject("http://localhost:" + port + "/jpa/users/andy/todos",
                String.class)).contains("\"Learn to Dance\"");
    }

    @Test
    @Tag("IntegrationTest")
    @Order(2)
    public void deleteFromListTodos() {
        this.restTemplate.withBasicAuth(USERNAME, PASSWORD).delete("http://localhost:" + port + "/jpa/users/andy/todo/10001");
        assertThat(this.restTemplate.withBasicAuth(USERNAME, PASSWORD).getForObject("http://localhost:" + port + "/jpa/users/andy/todo",
                String.class)).doesNotContain("\"Learn to Dance\"");
    }

    @Test
    @Tag("IntegrationTest")
    @Order(3)
    public void getTodo() {
        assertThat(this.restTemplate.withBasicAuth(USERNAME, PASSWORD).getForObject("http://localhost:" + port + "/jpa/users/andy/todo/10002",
                String.class)).contains("\"Learn to Cook\"");
    }

    @Test
    @Tag("IntegrationTest")
    @Order(4)
    public void updateTodo() {
        Todo todo = new Todo(10003, "andy", "Update Test", new Date(), false);

        this.restTemplate.withBasicAuth(USERNAME, PASSWORD).put("http://localhost:" + port + "/jpa/users/andy/todo/10003", todo);
        assertThat(this.restTemplate.withBasicAuth(USERNAME, PASSWORD).getForObject("http://localhost:" + port + "/jpa/users/andy/todo/10003",
                String.class)).contains("\"Update Test\"");
    }

    @Test
    @Tag("IntegrationTest")
    @Order(5)
    public void createATodo() {

        JSONObject obj = new JSONObject();
        obj.put("username", "andy");
        obj.put("description", "Learn to Ride a Bicycle");
        obj.put("targetDate", 1607969575);
        obj.put("done", false);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request =
                new HttpEntity<String>(obj.toString(), headers);

        String result = this.restTemplate.withBasicAuth(USERNAME, PASSWORD).postForObject("http://localhost:"+port +"/jpa/users/andy/todo", request, String.class);
        System.out.println(result);
        assertThat(this.restTemplate.withBasicAuth(USERNAME, PASSWORD).getForObject("http://localhost:"+port+"/jpa/users/andy/todos",
                String .class)).contains("Learn to Ride a Bicycle");
    }
}
