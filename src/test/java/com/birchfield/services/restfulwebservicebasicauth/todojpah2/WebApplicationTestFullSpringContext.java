package com.birchfield.services.restfulwebservicebasicauth.todojpah2;

//Using MVCMock
//useful approach is to not start the server at all but to test only the
// layer below that, where Spring handles the incoming HTTP request and
// hands it off to your controller. That way, almost of the full
// stack is used, and your code will be called in exactly the same
// way as if it were processing a real HTTP request but without the
// cost of starting the server.
//https://spring.io/guides/gs/testing-web/
/*********
//the full Spring application context is started but without the server.
*/

import com.birchfield.services.restfulwebservicebasicauth.todo.Todo;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Date;

import static com.birchfield.services.restfulwebservicebasicauth.TestUtils.asJsonString;
import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//Spring Context Test
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
public class WebApplicationTestFullSpringContext {

    private static String USERNAME = "andy";
    private static String PASSWORD = "password";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @Before
    public void setup() {
        //https://www.baeldung.com/spring-security-integration-tests
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .build();
    }

    @WithMockUser("anyuser")
    @Test
    @Tag("IntegrationTest")
    public void shouldReturnListTodos() throws Exception {
        this.mockMvc.perform(get("/jpa/users/andy/todos")).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Learn to Dance")));
    }

    @WithMockUser("anyuser")
    @Test
    @Tag("IntegrationTest")
    public void deleteATodo() throws Exception {
        this.mockMvc.perform(delete("/jpa/users/andy/todo/10002")).andDo(print())
                .andExpect(status().isNoContent());
    }

    @WithMockUser("anyuser")
    @Test
    @Tag("IntegrationTest")
    public void shouldReturnTodo() throws Exception {
        this.mockMvc.perform(get("/jpa/users/andy/todo/10003")).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Learn about Microservices")));
    }

    @WithMockUser("anyuser")
    @Test
    @Tag("IntegrationTest")
    public void shouldUpdateTodo() throws Exception {
        Todo todo = new Todo(10004, "andy", "Update Test", new Date(), false);

        this.mockMvc.perform(put("/jpa/users/andy/todo/10004")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(todo)))
            .andDo(print())
                .andExpect(status().isOk());
        this.mockMvc.perform(get("/jpa/users/andy/todo/10004")).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Update Test")));
    }

    @WithMockUser("anyuser")
    @Test
    @Tag("IntegrationTest")
    public void doesNotUpdateTodoWhereIDsDoNotMatch() throws Exception {
        Todo todo = new Todo(10004, "andy", "Update Test", new Date(), false);
        this.mockMvc.perform(put("/jpa/users/andy/todo/10003")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(todo)))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("The id in the URI did not match the id in the request")));
    }

    @WithMockUser("anyuser")
    @Test
    @Tag("IntegrationTest")
    public void shouldCreateTodo() throws Exception {

        JSONObject todo = new JSONObject();
        todo.put("username", "andy");
        todo.put("description", "Learn to Walk and Run");
        todo.put("targetDate", 1607969575);
        todo.put("done", false);

        this.mockMvc.perform(post("/jpa/users/andy/todo")
                .contentType(MediaType.APPLICATION_JSON)
                .content(todo.toString()))
                .andDo(print())
                .andExpect(status().isCreated());
        this.mockMvc.perform(get("/jpa/users/andy/todos"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Learn to Walk and Run")));
    }


}
