package com.birchfield.services.restfulwebservicebasicauth;

import com.birchfield.services.restfulwebservicebasicauth.todo.TodoController;
import com.birchfield.services.restfulwebservicebasicauth.todo.TodoHardCodedService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//Spring Web Handling Layer Test
@WebMvcTest(TodoController.class)
public class OriginTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TodoHardCodedService service;

    @WithMockUser("anyuser")
    @Test
    public void shouldAllowCrossOrigin() throws Exception {
        ResultActions actions = mockMvc.perform(
                get("/users/test/todos")
                        .contentType(MediaType.APPLICATION_JSON)
                        //CORS HEADERS
                        //.header("Access-Control-Request-Method", "GET")
                        .header("Origin", "http://localhost:4200")
        );
        actions.andExpect(status().isOk());
    }

    @WithMockUser("anyuser")
    @Test
    public void notAllowCrossOrigin() throws Exception {
        ResultActions actions = mockMvc.perform(
                get("/users/test/todos")
                        .contentType(MediaType.APPLICATION_JSON)
                        //CORS HEADERS
                        //.header("Access-Control-Request-Method", "GET")
                        .header("Origin", "http://anotherdomain:4200")
        );
        actions.andExpect(status().isForbidden());
    }
}
