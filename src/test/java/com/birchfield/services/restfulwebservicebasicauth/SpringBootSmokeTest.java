package com.birchfield.services.restfulwebservicebasicauth;

import com.birchfield.services.restfulwebservicebasicauth.todo.TodoController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class SpringBootSmokeTest {

	@Autowired
	private TodoController controller;

	//a simple sanity check test that will fail if the application context cannot start
	@Test
	void contextLoads() {
		System.out.print("testing context");
		assertThat(controller).isNotNull();
	}

}
