package com.birchfield.services.restfulwebservicebasicauth.helloworld;

//Using MVCMock
//useful approach is to not start the server at all but to test only the
// layer below that, where Spring handles the incoming HTTP request and
// hands it off to your controller. That way, almost of the full
// stack is used, and your code will be called in exactly the same
// way as if it were processing a real HTTP request but without the
// cost of starting the server.

/*********
//narrow the tests to only the web layer by using @WebMvcTest,
 The test assertion is the same as in the previous case (WebApplicationTestFullSpringContext).
 However, in this test, Spring Boot instantiates only the web layer rather
 than the whole context. In an application with multiple controllers, you
 can even ask for only one to be instantiated by using, for example,
*/
//https://www.baeldung.com/spring-security-integration-tests
//https://docs.spring.io/spring-security/site/docs/5.2.x/reference/html/test.html

import org.junit.Before;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//Spring Web Handling Layer Test
@WebMvcTest(HelloWorldController.class)
public class WebApplicationTestWebLayerSpringSecurityContext {
    private static String USERNAME = "andy";
    private static String PASSWORD = "password";

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private MockMvc mockMvc;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .build();
    }

    @WithMockUser(value = "anyuser")
    @Test
    @Tag("IntegrationTest")
    public void shouldPass() throws Exception {
        this.mockMvc.perform(get("/helloworld"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @Tag("IntegrationTest")
    public void shouldPassValidUserandPasswordSupplied() throws Exception {
        //no mockuser annotation used
        this.mockMvc.perform(get("/helloworld")
                .with(httpBasic(USERNAME,PASSWORD)))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @Tag("IntegrationTest")
    public void shouldFailNoUserNameSupplied() throws Exception {
        //this work b/c the spring context is loaded in some sort of quick mock way ?
        this.mockMvc.perform(get("/helloworld")).andDo(print())
                .andExpect(status().isUnauthorized());
    }

    @Test
    @Tag("IntegrationTest")
    public void shouldFailInvalidUser() throws Exception {
        //this work b/c the spring context is loaded in some sort of quick mock way ?
        this.mockMvc.perform(get("/helloworld")
                .with(httpBasic("invaliduser",PASSWORD)))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }

    @Test
    @Tag("IntegrationTest")
    public void shouldFailInvalidPassword() throws Exception {
        //this work b/c the spring context is loaded in some sort of quick mock way ?
        this.mockMvc.perform(get("/helloworld")
                .with(httpBasic(USERNAME,"invalidPassword")))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }
}
