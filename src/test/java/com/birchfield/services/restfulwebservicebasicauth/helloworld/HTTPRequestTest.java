package com.birchfield.services.restfulwebservicebasicauth.helloworld;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

//Tests via HTTP layer
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class HTTPRequestTest {

    private static String USERNAME = "andy";
    private static String PASSWORD = "password";

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    @Tag("IntegrationTest")
    public void greetingShouldReturnDefaultMessage() throws Exception {
        assertThat(this.restTemplate.withBasicAuth(USERNAME, PASSWORD).getForObject("http://localhost:" + port + "/helloworld",
                String.class)).contains("Hello World");
    }

    @Test
    @Tag("IntegrationTest")
    public void invalidUser401() throws Exception {
        //https://www.baeldung.com/spring-security-integration-tests
        ResponseEntity<String> result = restTemplate.withBasicAuth("invaliduser", PASSWORD)
                .getForEntity("/helloworld", String.class);
        assertEquals(HttpStatus.UNAUTHORIZED, result.getStatusCode());
    }

    @Test
    @Tag("IntegrationTest")
    public void invalidPassword401() throws Exception {
        //https://www.baeldung.com/spring-security-integration-tests
        ResponseEntity<String> result = restTemplate.withBasicAuth(USERNAME, "invalidpassword")
                .getForEntity("/helloworld", String.class);
        assertEquals(HttpStatus.UNAUTHORIZED, result.getStatusCode());
    }

    @Test
    @Tag("IntegrationTest")
    public void greetingShouldReturnDefaultObject() throws Exception {
        String actual = this.restTemplate.withBasicAuth(USERNAME, PASSWORD).getForObject("http://localhost:" + port + "/helloworldbean",
                String.class);
        assertThat(actual).isEqualTo("{\"message\":\"Hello World Bean\"}");
    }

    @Test
    @Tag("IntegrationTest")
    public void greetingShouldReturnDefaultObjectPathVariable() throws Exception {
        String actual = this.restTemplate.withBasicAuth(USERNAME, PASSWORD).getForObject("http://localhost:" + port + "/helloworldbean/andy",
                String.class);
        assertThat(actual).isEqualTo("{\"message\":\"Hello World Bean andy\"}");
    }
}