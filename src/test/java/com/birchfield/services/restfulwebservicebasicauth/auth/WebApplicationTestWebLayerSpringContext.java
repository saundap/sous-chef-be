package com.birchfield.services.restfulwebservicebasicauth.auth;

//Using MVCMock
//useful approach is to not start the server at all but to test only the
// layer below that, where Spring handles the incoming HTTP request and
// hands it off to your controller. That way, almost of the full
// stack is used, and your code will be called in exactly the same
// way as if it were processing a real HTTP request but without the
// cost of starting the server.

/*********
//narrow the tests to only the web layer by using @WebMvcTest,
 The test assertion is the same as in the previous case (WebApplicationTestFullSpringContext).
 However, in this test, Spring Boot instantiates only the web layer rather
 than the whole context. In an application with multiple controllers, you
 can even ask for only one to be instantiated by using, for example,
*/

import com.birchfield.services.restfulwebservicebasicauth.auth.basic.BasicAuthenticationController;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//Spring Web Handling Layer Test
@WebMvcTest(BasicAuthenticationController.class)
public class WebApplicationTestWebLayerSpringContext {

    @Autowired
    private MockMvc mockMvc;


    @WithMockUser(value = "anyuser")   //prevents auth failures on test run any user value is fine
    @Test
    @Tag("UnitTest")
    public void shouldBeAuthenticated() throws Exception {
        this.mockMvc.perform(get("/basicauth")).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("You are authenticated")))
                .andExpect(authenticated());
    }

    @Test
    @Tag("UnitTest")
    public void shouldBeUnAuthenticated() throws Exception {
        this.mockMvc.perform(get("/basicauth")).andDo(print())
                .andExpect(status().isUnauthorized())
                .andExpect(unauthenticated());
    }
}
