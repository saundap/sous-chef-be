package com.birchfield.services.restfulwebservicebasicauth.auth;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

import static org.assertj.core.api.Assertions.assertThat;

//Tests via HTTP layer
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class HTTPRequestTest {

    private static String USERNAME = "andy";
    private static String PASSWORD = "password";

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    @Tag("IntegrationTest")
    public void greetingShouldReturnDefaultMessage() throws Exception {
        assertThat(this.restTemplate.withBasicAuth(USERNAME, PASSWORD).getForObject("http://localhost:" + port + "/basicauth",
                String.class)).contains("You are authenticated");
    }

}
