package com.birchfield.services.restfulwebservicebasicauth.todo;

//Using MVCMock
//useful approach is to not start the server at all but to test only the
// layer below that, where Spring handles the incoming HTTP request and
// hands it off to your controller. That way, almost of the full
// stack is used, and your code will be called in exactly the same
// way as if it were processing a real HTTP request but without the
// cost of starting the server.
//https://spring.io/guides/gs/testing-web/
/*********
//narrow the tests to only the web layer by using @WebMvcTest,
 The test assertion is the same as in the previous case (WebApplicationTestFullSpringContext).
 However, in this test, Spring Boot instantiates only the web layer rather
 than the whole context. In an application with multiple controllers, you
 can even ask for only one to be instantiated by using, for example,
 @WebMvcTest(HomeController.class).

 @WebMvcTest is only going to scan the controller you've defined and the MVC infrastructure.
 That's it. So if your controller has some dependency to other beans from your service layer,
 the test won't start until you either load that config yourself or provide a mock for it. This
 is much faster as we only load a tiny portion of your app. This annotation uses slicing.
*/

//https://www.baeldung.com/spring-security-integration-tests
//https://docs.spring.io/spring-security/site/docs/5.2.x/reference/html/test.html

import com.birchfield.services.restfulwebservicebasicauth.todo.Todo;
import com.birchfield.services.restfulwebservicebasicauth.todo.TodoController;
import com.birchfield.services.restfulwebservicebasicauth.todo.TodoHardCodedService;
import org.junit.Before;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//Spring Web Handling Layer Test
@WebMvcTest(TodoController.class)
public class WebApplicationTestWebLayerSpringSecurityContext {
    private static String USERNAME = "andy";
    private static String PASSWORD = "password";
    private static List<Todo> todos = new ArrayList();
    private static Todo todo = new Todo(0, "andy", "Learn about MockBeans", new Date(), false);

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TodoHardCodedService mockService;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .build();

        todos.add(todo);

    }

    @WithMockUser("anyuser")
    @Test
    @Tag("UnitTest")
    public void shouldPassUsingWithMockUser() throws Exception {
        List<Todo> todos = new ArrayList();
        todos.add(new Todo(0, "andy", "Learn about MockBeans", new Date(), false));

        when(mockService.findAll()).thenReturn(todos);
        this.mockMvc.perform(get("/users/andy/todos")).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Learn about MockBeans")));

        verify(mockService, atLeast(1)).findAll();

    }

    @Test
    @Tag("UnitTest")
    public void shouldWithHttpBasicHeader() throws Exception {
        List<Todo> todos = new ArrayList();
        todos.add(new Todo(0, "andy", "Learn about MockBeans", new Date(), false));
        when(mockService.findAll()).thenReturn(todos);
        this.mockMvc.perform(get("/users/andy/todos")
                .with(httpBasic(USERNAME,PASSWORD))).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Learn about MockBeans")));
    }

    @Test
    @Tag("UnitTest")
    public void shouldFailNoUserNameSupplied() throws Exception {
        when(mockService.findAll()).thenReturn(todos);
        this.mockMvc.perform(get("/users/andy/todos")).andDo(print())
                .andExpect(status().isUnauthorized());
    }

    @Test
    @Tag("UnitTest")
    public void shouldFailInvalidUser() throws Exception {
        when(mockService.findAll()).thenReturn(todos);
        this.mockMvc.perform(get("/users/andy/todos")
                .with(httpBasic("invalidusername",PASSWORD))).andDo(print())
                .andExpect(status().isUnauthorized());
    }

    @Test
    @Tag("UnitTest")
    public void shouldFailInvalidPassword() throws Exception {
        when(mockService.findAll()).thenReturn(todos);
        this.mockMvc.perform(get("/users/andy/todos")
                .with(httpBasic(USERNAME,"invalidpassword"))).andDo(print())
                .andExpect(status().isUnauthorized());
    }
}