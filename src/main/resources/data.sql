--Todo Insert Data for H2 SQL JPA
--make sure the insert statement matches the h2 table names
--(camelcase-named class variables from the todo entity are translated with underscores by JPA when db is created

insert into todojpa(id,username,description,target_date,is_done)
values(10001,'andy', 'Learn to Dance', sysdate(), false);

insert into todojpa(id,username,description,target_date,is_done)
values(10002,'andy', 'Learn to Cook', sysdate(), false);

insert into todojpa(id,username,description,target_date,is_done)
values(10003,'andy', 'Learn about Microservices', sysdate(), false);

insert into todojpa(id,username,description,target_date,is_done)
values(10004,'andy', 'Learn about React', sysdate(), false);

insert into todojpa(id,username,description,target_date,is_done)
values(10005,'andy', 'Learn about SpringBoot', sysdate(), false);

insert into todojpa(id,username,description,target_date,is_done)
values(10006,'andy', 'Learn about JPA', sysdate(), false);

insert into todojpa(id,username,description,target_date,is_done)
values(10007,'andy', 'Learn Data JPA', sysdate(), false);