package com.birchfield.services.restfulwebservicejwtauth.helloworld;

public class HelloWorldBean {

    private String testMessage;

    public HelloWorldBean(String message) {
        this.testMessage = message;
    }

    public String getMessage() {
        return testMessage;
    }

    public void setMessage() {
        this.testMessage = testMessage;
    }

    @Override
    public String toString() {
        return String.format("HelloWorldBean [testMessage=%s]", testMessage);
    }


}
