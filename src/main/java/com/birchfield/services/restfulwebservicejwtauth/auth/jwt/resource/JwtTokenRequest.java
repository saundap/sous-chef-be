package com.birchfield.services.restfulwebservicejwtauth.auth.jwt.resource;

import java.io.Serializable;

public class  JwtTokenRequest implements Serializable {

    private static final long serialVersionUID = -5616176897013108345L;

    private String username;
    private String password;

    //token response from POST /authenticate with body {"username":"andy,"password":"password"}
    //{
    //    "token": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhbmR5IiwiZXhwIjoxNjA4ODIwMTkzLCJpYXQiOjE2MDgyMTUzOTN9.VX4cDxYHTOmNbv8qZoGyhJYqcVm9V6xuwzDofMkdIDafejz1dCddmG-dR3nW9623rUQu2JFlV0fJQ52bKLbr-w"
    //}
    //interpret it at jwt.io
    //secret key is in the application.properties as jwt.signing.key.secret
    //it should be at least 64bit in lenght for a production system

    public JwtTokenRequest() {
        super();
    }

    public JwtTokenRequest(String username, String password) {
        this.setUsername(username);
        this.setPassword(password);
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

