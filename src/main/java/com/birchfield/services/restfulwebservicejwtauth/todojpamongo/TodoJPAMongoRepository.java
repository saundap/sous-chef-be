package com.birchfield.services.restfulwebservicejwtauth.todojpamongo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TodoJPAMongoRepository
            extends MongoRepository<TodoJPAMongo, Long> {

    //https://www.baeldung.com/queries-in-spring-data-mongodb
    //Spring Boot JPA Magic -
    TodoJPAMongo findById(long id);
    List<TodoJPAMongo> findByUsername(String username);
    List<TodoJPAMongo> findByUsernameStartingWith(String regexp);
    List<TodoJPAMongo> findByUsernameEndingWith(String regexp);
}
