package com.birchfield.services.restfulwebservicejwtauth.todojpamongo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Random;

import static org.springframework.data.mongodb.core.query.Criteria.where;

//https://reflectoring.io/spring-boot-web-controller-test/
//https://www.javadevjournal.com/spring-boot/spring-boot-with-mongodb/
@RestController
public class TodoJPAMongoController {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final TodoJPAMongoRepository todoJPAMongoRepository;

    @Autowired
    private MongoTemplate mongoTemplate;

    public TodoJPAMongoController(TodoJPAMongoRepository todoJPAMongoRepository) {
        //new LoadMongoDBTodoData();
        this.todoJPAMongoRepository = todoJPAMongoRepository;
    }

    @GetMapping("/jpa/mongo/users/{username}/todos")
    public List<TodoJPAMongo> getAllTodos(@PathVariable String username){
        logger.info("Getting all Todos");
        return todoJPAMongoRepository.findByUsername(username);
    }

    @GetMapping("/jpa/mongo/users/{username}/todo/{id}")
    public TodoJPAMongo getTodo(@PathVariable String username, @PathVariable long id){
        System.out.println("calling todoJPAMongoRepository Service instance and findById method");
        return todoJPAMongoRepository.findById(id);
    }

    @DeleteMapping("/jpa/mongo/users/{username}/todo/{id}")
    public ResponseEntity<Void> deleteToDo(@PathVariable String username, @PathVariable long id){
        System.out.println("deleteToDo");

        Query query = new Query(where("id").is(id));
        mongoTemplate.findAndRemove(query,TodoJPAMongo.class);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/jpa/mongo/users/{username}/todo/{id}")
    public ResponseEntity<TodoJPAMongo> updateTodo(
                                            @PathVariable String username,
                                            @PathVariable long id,
                                            @RequestBody TodoJPAMongo todoJPAMongo){
        System.out.println("updateTodo");

        //We can change the method type here to Todo and just return the Todo object, as default is http ok.
        //But we've prepared ourselves here to return other ResponseEntity types in the future with different status if need be
        if(id != todoJPAMongo.getId()){
            //error handling must be better, this set approach puts the errormessage property into every object on create
            //https://www.baeldung.com/exception-handling-for-rest-with-spring
            todoJPAMongo.setErrormessage("The id in the URI did not match the id in the request");
            return new ResponseEntity<>(todoJPAMongo, HttpStatus.BAD_REQUEST);
        }
        System.out.println("here1");

        Update update=new Update();
            update.set("description",todoJPAMongo.getDescription());
            update.set("targetDate",todoJPAMongo.getTargetDate());
            update.set("username",username);
        System.out.println("here2");

        Query query = new Query(where("id").is(id));
        mongoTemplate.updateFirst(query,update,TodoJPAMongo.class);
        System.out.println("here3");

        return new ResponseEntity<>(todoJPAMongoRepository.findById(id), HttpStatus.OK);
    }

    @PostMapping("/jpa/mongo/users/{username}/todo")
    public ResponseEntity<TodoJPAMongo> createTodo(
                                            @PathVariable String username,
                                            @RequestBody TodoJPAMongo todoJPAMongo){
        todoJPAMongo.setUsername("test");
        Random random = new Random();
        // generate random integer value
        // within 100 inclusive and 500 exclusive
        //todo: identify the highest current id number in db and increment
        int num = random.ints(1, 50000).findFirst().getAsInt();
        todoJPAMongo.setId(num);
            TodoJPAMongo createdTodo = todoJPAMongoRepository.save(todoJPAMongo);

            //return the location of the current resource
            //users/{username}/todo/{id}
            URI uri = ServletUriComponentsBuilder
                    .fromCurrentRequest()
                    .path("/{id}")
                    .buildAndExpand(createdTodo.getId())
                    .toUri();

            //returns a Location header value in the response header
            // created returns a 201
            return ResponseEntity.created(uri).build();
        }

}
