package com.birchfield.services.restfulwebservicejwtauth.dishes.services;

import com.birchfield.services.restfulwebservicejwtauth.dishes.entities.Dish;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DishesRepository
            extends MongoRepository<Dish, Long> {

    //https://www.baeldung.com/queries-in-spring-data-mongodb
    //Spring Boot JPA Magic -
    Dish findById(long id);
    List<Dish> findByDish(String dish);
    List<Dish> findByIngredientsContaining(String regexp);
    List<Dish> findByDishStartingWith(String regexp);
    List<Dish> findByDishEndingWith(String regexp);
}
