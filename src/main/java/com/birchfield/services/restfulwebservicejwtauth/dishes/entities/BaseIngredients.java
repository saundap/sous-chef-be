package com.birchfield.services.restfulwebservicejwtauth.dishes.entities;

public class BaseIngredients {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "BaseIngredients{" +
                "name='" + name + '\'' +
                '}';
    }
}
