package com.birchfield.services.restfulwebservicejwtauth.dishes.controllers;

import com.birchfield.services.restfulwebservicejwtauth.dishes.entities.CookTime;
import com.birchfield.services.restfulwebservicejwtauth.dishes.services.DishesRepository;
import com.birchfield.services.restfulwebservicejwtauth.dishes.entities.Dish;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

import static org.springframework.data.mongodb.core.query.Criteria.where;

@RestController
public class DishesController {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final DishesRepository dishesRepository;

    @Autowired
    private MongoTemplate mongoTemplate;

    public DishesController(DishesRepository dishesRepository) {
        this.dishesRepository = dishesRepository;
    }

    @GetMapping(value = "/dishes/{id}")
    public Dish getDish(@PathVariable long id){
        logger.info("Getting a dish");
        return dishesRepository.findById(id);
    }

    @GetMapping(value = "/dishes")
    public List<Dish> getDishesWithIngredient(@RequestParam(required = false, value="ingredient") String ingredient){
        if (ingredient == null) {
            logger.info("Getting all Dishes");
            return dishesRepository.findAll();
        }else{
            logger.info("Finding dishes with shared ingredient {}", ingredient);
            Query query = new Query(where("ingredients.name").regex(ingredient));
            List<Dish> dishes = mongoTemplate.find(query, Dish.class);
            //List<Dish> dishes = dishesRepository.findByIngredientsContaining(ingredient);
            logger.info("Found {} dishes with the {}", dishes.size(), ingredient);

            return dishes;
        }
    }

    @PostMapping("/dishes")
    public ResponseEntity<Dish> createDish(
            //@PathVariable String username,
            @RequestBody Dish dish){
        if(dish.getDish().isEmpty()){
            //https://www.baeldung.com/exception-handling-for-rest-with-spring
            return new ResponseEntity<>(dish, HttpStatus.BAD_REQUEST);
        }

        int greatestId = getHighestId();

        logger.info("PostMapping");
        logger.info("Saving");
        logger.info(String.valueOf(greatestId+1));
        dish.setId(greatestId+1);
        CookTime cookTime = new CookTime();
        cookTime.setTime(dish.getCookTime().getTime());
        cookTime.setType("minutes");
        dish.setCookTime(cookTime);
        logger.info(dish.toString());

        dishesRepository.save(dish);


        //return the location of the current resource
        // /dishes/{id}
        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(greatestId+1)
                .toUri();

        //returns a Location header value in the response header
        // created returns a 201
        return ResponseEntity.created(uri).build();
    }

    private int getHighestId(){
        Query query = new Query();
        query.with(Sort.by(Sort.Direction.DESC, "id"));
        query.limit(1);
        Dish dishIdGreatest = mongoTemplate.findOne(query, Dish.class);
        int currentId = dishIdGreatest.getId();
        logger.info("The highest record id is {}",String.valueOf(currentId));
        return currentId;
    }


}
