package com.birchfield.services.restfulwebservicejwtauth.dishes.entities;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.persistence.GeneratedValue;
import java.util.List;
import java.util.Objects;

@Document(collection = "dishes")
public class Dish {

    @Field("id")
    @GeneratedValue
    private int id;

    private String dish;
    private String type;
    private CookTime cookTime;
    private String equipment;
    private List<BaseIngredients> baseIngredients;
    private List<Ingredients> ingredients;

    protected Dish(){}

    public Dish(int id, String dish, String type, CookTime cookTime, String equipment, List<BaseIngredients> baseIngredients, List<Ingredients> ingredients) {
        super();
        this.id = id;
        this.dish = dish;
        this.type = type;
        this.cookTime = cookTime;
        this.equipment = equipment;
        this.baseIngredients = baseIngredients;
        this.ingredients = ingredients;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDish() {
        return dish;
    }

    public void setDish(String dish) {
        this.dish = dish;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public CookTime getCookTime() {
        return cookTime;
    }

    public void setCookTime(CookTime cookTime) {
        this.cookTime = cookTime;
    }

    public String getEquipment() {
        return equipment;
    }

    public void setEquipment(String equipment) {
        this.equipment = equipment;
    }

    public List<BaseIngredients> getBaseIngredients() {
        return baseIngredients;
    }

    public void setBaseIngredients(List<BaseIngredients> baseIngredients) {
        this.baseIngredients = baseIngredients;
    }

    public List<Ingredients> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Ingredients> ingredients) {
        this.ingredients = ingredients;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Dish dish = (Dish) o;
        return id == dish.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Dish{" +
                "id=" + id +
                ", dish='" + dish + '\'' +
                ", type='" + type + '\'' +
                ", cookTime=" + cookTime.toString() +
                ", equipment='" + equipment + '\'' +
                ", baseIngredients=" + baseIngredients.toString() +
                ", ingredients=" + ingredients.toString() +
                '}';
    }
}
