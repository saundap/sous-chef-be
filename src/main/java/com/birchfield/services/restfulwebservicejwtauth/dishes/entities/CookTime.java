package com.birchfield.services.restfulwebservicejwtauth.dishes.entities;

public class CookTime {
    private int time;
    private String type;

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "CookTime{" +
                "time=" + time +
                ", type='" + type + '\'' +
                '}';
    }
}
