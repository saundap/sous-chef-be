package com.birchfield.services.restfulwebservicejwtauth.todojpah2;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TodoJPAH2Repository extends JpaRepository<TodoJPA, Long> {

    //Spring Boot JPA Magic -
    List<TodoJPA> findByUsername(String username);
}
