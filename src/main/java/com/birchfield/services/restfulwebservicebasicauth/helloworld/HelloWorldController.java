package com.birchfield.services.restfulwebservicebasicauth.helloworld;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
//@CrossOrigin(origins="http://localhost:4200") //for granular cross origin setting - see class WebConfig for global setting
public class HelloWorldController {

    //method - returns "Hello World"
    //GET
    //URI - /helloworld
    //@RequestMapping(method = RequestMethod.GET, path = "/helloworld")
    @GetMapping(path = "/helloworld")
    public @ResponseBody String helloWorld(){
        return "Hello World";
    }

    //create and return a bean
    @GetMapping(path = "/helloworldbean")
    public @ResponseBody
    HelloWorldBean helloWorldBean(){
        return new HelloWorldBean("Hello World Bean");
    }

    //create and return a bean with path variable
    @GetMapping(path = "/helloworldbean/{name}")
    public @ResponseBody
    HelloWorldBean helloWorldPathVariableBean(@PathVariable String name){
        return new HelloWorldBean(String.format("Hello World Bean %s", name));
    }

    //create and return a bean with path variable
    @GetMapping(path = "/helloworldbeanerror/{name}")
    public @ResponseBody
    HelloWorldBean helloWorldPathVariableBeanERROR(@PathVariable String name){
        throw new RuntimeException("Something went wrong with HelloWorldBean service");
    }

}
