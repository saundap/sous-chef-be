package com.birchfield.services.restfulwebservicebasicauth.todojpah2;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TodoJPARepository extends JpaRepository<TodoJPA, Long> {

    //Spring Boot JPA Magic -
    List<TodoJPA> findByUsername(String username);
}
