package com.birchfield.services.restfulwebservicebasicauth.todojpah2;

import com.birchfield.services.restfulwebservicebasicauth.todo.Todo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

//https://reflectoring.io/spring-boot-web-controller-test/
@RestController
public class TodoJPAController {

    @Autowired
    private TodoJPARepository todoJPARepository;

    @GetMapping("/jpa/users/{username}/todos")
    public List<TodoJPA> getAllTodos(@PathVariable String username){
        System.out.println("calling todoJpaRepository Service instance and findall method");
        //return todoService.findAll(); //disabled hardcoded service return

        //JPARepo implementation we can search by the username we did not use in the TodoHardCodedService implementation
        //Create a new 'service method in the TodoJpaRepository interface
        // and limit response by username which we did not do with hardcoded service
        return todoJPARepository.findByUsername(username);
    }

    @GetMapping("/jpa/users/{username}/todo/{id}")
    public TodoJPA getTodo(@PathVariable String username, @PathVariable long id){
        System.out.println("calling todoJpaRepository Service instance and findById method");
        //return todoService.findById(id); //disabled hardcoded service return
        return todoJPARepository.findById(id).get(); //.get() b/c findById returns optional, get allows us to return our Object TodoJPA defined by our TodoJapRepository interface.
    }

    @DeleteMapping("/jpa/users/{username}/todo/{id}")
    public ResponseEntity<Void> deleteToDo(@PathVariable String username, @PathVariable long id){
        todoJPARepository.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/jpa/users/{username}/todo/{id}")
    public ResponseEntity<TodoJPA> updateTodo(
                                            @PathVariable String username,
                                            @PathVariable long id,
                                            @RequestBody TodoJPA todoJPA){
        //We can change the method type here to Todo and just return the Todo object, as defualt is http ok.
        //But we've prepared ourselves here to return other ResponseEntity types in the future with different status if need be
        if(id != todoJPA.getId()){
            //error handling must be better, this set approach puts the errormessage property into every object on create
            //https://www.baeldung.com/exception-handling-for-rest-with-spring
            todoJPA.setErrormessage("The id in the URI did not match the id in the request");
            return new ResponseEntity<>(todoJPA, HttpStatus.BAD_REQUEST);
        }
        todoJPA.setUsername(username);
        TodoJPA todoUpdated = todoJPARepository.save(todoJPA);
        System.out.println(todoUpdated.getDescription());
        return new ResponseEntity<>(todoUpdated, HttpStatus.OK);
    }

    @PostMapping("/jpa/users/{username}/todo")
    public ResponseEntity<Todo> createTodo(
                                            @PathVariable String username,
                                            @RequestBody TodoJPA todoJPA){

        todoJPA.setUsername(username);
        TodoJPA createdTodo = todoJPARepository.save(todoJPA);

        //return the location of the current resource
        //users/{username}/todo/{id}
        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(createdTodo.getId())
                .toUri();

        //returns a Location header value in the response header
        // created returns a 201
        return ResponseEntity.created(uri).build();
    }

}
