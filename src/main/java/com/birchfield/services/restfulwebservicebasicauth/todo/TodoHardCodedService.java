package com.birchfield.services.restfulwebservicebasicauth.todo;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class TodoHardCodedService {

    private List<Todo> todos = new ArrayList();

    private int idCounter = 1;

    public TodoHardCodedService() {
        todos.add(new Todo(idCounter++, "andy", "Learn to Dance", new Date(), false));
        todos.add(new Todo(idCounter++, "andy", "Learn to Cook", new Date(), false));
        todos.add(new Todo(idCounter++, "andy", "Learn about Microservices", new Date(), false));
        todos.add(new Todo(idCounter++, "andy", "Learn about React", new Date(), false));
        todos.add(new Todo(idCounter++, "andy", "Learn about SpringBoot", new Date(), false));
    }

    public List<Todo> findAll(){
        return todos;
    }

    public Todo save(Todo todo){
        if(todo.getId() == -1 || todo.getId() == 0 ){ //POST case: check if no todo returned (-1) or if this is default todo (0), then create id and add on exit
            System.out.println("creating a new record");
            todo.setId(++idCounter);
        }else{ //PUT case: update by delete, then add on exit
            System.out.println("updating an existing record");
            deleteById(todo.getId());
        }
        todos.add(todo);
        return todo;
    }

    //Delete a Todo and return it back as a message
    public Todo deleteById(long id){
        Todo todo = findById(id);
        if(todo==null) return null;
        if(todos.remove(todo)){
            return todo;
        }
        return null;
    }

    public Todo findById(long id) {
        for(Todo todo : todos){
            if(todo.getId()== id){
                return todo;
            }
        }
        return null;
    }

}
