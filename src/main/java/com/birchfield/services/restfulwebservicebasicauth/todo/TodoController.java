package com.birchfield.services.restfulwebservicebasicauth.todo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

//https://reflectoring.io/spring-boot-web-controller-test/
@RestController
public class TodoController {
    final Logger logger = LoggerFactory.getLogger(com.birchfield.services.restfulwebservicejwtauth.todo.TodoController.class);

    @Autowired
    private TodoHardCodedService todoService;

    @GetMapping("/users/{username}/todos")
    public List<Todo> getAllTodos(@PathVariable String username){
        logger.info("GetMapping getAllTodos");

        System.out.println("calling todoService instance and findall method");

        return todoService.findAll();
    }

    @GetMapping("/users/{username}/todo/{id}")
    public Todo getTodo(@PathVariable String username, @PathVariable long id){
        return todoService.findById(id);
    }

    @DeleteMapping("/users/{username}/todo/{id}")
    public ResponseEntity<Void> deleteToDo(@PathVariable String username, @PathVariable long id){
        Todo todo = todoService.deleteById(id);
        if(todo!=null){
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.notFound().build();
    }

    @PutMapping("/users/{username}/todo/{id}")
    public ResponseEntity<Todo> updateTodo(
                                            @PathVariable String username,
                                            @PathVariable long id,
                                            @RequestBody Todo todo){
        //We can change the method type here to Todo and just return the Todo object, as defualt is http ok.
        //But we've prepared ourselves here to return other ResponseEntity types in the future with different status if need be
        if(id != todo.getId()){
            //error handling must be better, this set approach puts the errormessage property into every object on create
            //https://www.baeldung.com/exception-handling-for-rest-with-spring
            todo.setErrormessage("The id in the URI did not match the id in the request");
            return new ResponseEntity<>(todo, HttpStatus.BAD_REQUEST);
        }
        Todo todoUpdated = todoService.save(todo);
        System.out.println(todoUpdated.getDescription());
        return new ResponseEntity<>(todoUpdated, HttpStatus.OK);
    }

    @PostMapping("/users/{username}/todo")
    public ResponseEntity<Todo> createTodo(
                                            @PathVariable String username,
                                            @RequestBody Todo todo){

        Todo createdTodo = todoService.save(todo);

        //return the location of the current resource
        //users/{username}/todo/{id}
        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(createdTodo.getId())
                .toUri();

        //returns a Location header value in the response header
        // created returns a 201
        return ResponseEntity.created(uri).build();
    }

}
