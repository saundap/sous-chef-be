package com.birchfield.services.restfulwebservicebasicauth.auth.basic;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BasicAuthenticationController {

    @GetMapping(path = "/basicauth")
    public @ResponseBody
    AuthenticationBean basicAuth(){
        return new AuthenticationBean("You are authenticated");
    }


}
