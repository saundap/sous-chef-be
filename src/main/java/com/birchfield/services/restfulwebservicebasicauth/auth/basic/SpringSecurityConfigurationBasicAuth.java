package com.birchfield.services.restfulwebservicebasicauth.auth.basic;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class SpringSecurityConfigurationBasicAuth extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //this.logger.debug("Using default configure(HttpSecurity). "
        //        + "If subclassed this will potentially override subclass configure(HttpSecurity).");

        //not sure i agree with this skipping all OPTIONS checks - why not just have an Auth check on the OPTIONS request, isn't that present as a method to use
        //for efficiency?
        //an OPTIONS check is sent before a request (once) to validate if it will be permitted before it sends data (efficiency thing)
        http
            .csrf().disable() //disable csrf checking b/c JWT takes care of this risk
            .authorizeRequests()
            .antMatchers(HttpMethod.OPTIONS, "/**").permitAll() //any OPTION request to any URL, permit anyone i.e. no authentication applied
                .anyRequest().authenticated() //any other request we authenticate with basic auth
                .and()
            //http.formLogin(); //get rid of this
            .httpBasic();
    }

//other methods we can override for other security authorisation methods
/*    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
                // Spring Security should completely ignore URLs starting with /resources/
                .antMatchers("/resources/**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().antMatchers("/public/**").permitAll().anyRequest()
                .hasRole("USER").and()
                // Possibly more configuration ...
                .formLogin() // enable form based log in
                // set permitAll for all URLs associated with Form Login
                .permitAll();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
                // enable in memory based authentication with a user named "user" and "admin"
                .inMemoryAuthentication().withUser("user").password("password").roles("USER")
                .and().withUser("admin").password("password").roles("USER", "ADMIN");
    }*/

}
