package com.birchfield.services.restfulwebservicebasicauth.auth.basic;

public class AuthenticationBean {

    private String message;

    public AuthenticationBean(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage() {
        this.message = message;
    }

    @Override
    public String toString() {
        return String.format("HelloWorldBean [testMessage=%s]", message);
    }


}
