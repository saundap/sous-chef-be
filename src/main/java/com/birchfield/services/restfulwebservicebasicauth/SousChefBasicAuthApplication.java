package com.birchfield.services.restfulwebservicebasicauth;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.context.ApplicationContext;

//https://stackoverflow.com/a/50813817
//not yet using mongodb
@SpringBootApplication(exclude = {MongoAutoConfiguration.class, MongoDataAutoConfiguration.class})
public class SousChefBasicAuthApplication {

	public static void main(String[] args) {
		final Logger logger = LoggerFactory.getLogger(SousChefBasicAuthApplication.class);

		ApplicationContext applicationContext = SpringApplication.run(SousChefBasicAuthApplication.class, args);

		logger.info("Logging out all the beans instantiated");
		logger.info("Set logging debug to TRUE to see similar with more details with conditional details listed");
		for(String name: applicationContext.getBeanDefinitionNames()){
			logger.info(name);
		}
	}

}
